﻿using System;

namespace Divvy.Models
{
    public interface IRefreshData
    {
        //PROPERTIES//
        string error { get; }
        DateTime? refreshTime { get; }
        string stationName { get; }
        int? availableDocks { get; }
        int? availableBikes { get; }
        DateTime? communicationTime { get; }
    }
}
