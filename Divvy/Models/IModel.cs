﻿using System;
using System.Threading.Tasks;

namespace Divvy.Models
{
    public interface IModel
    {
        //METHODS//
        Task startMonitoringAsync();
        Task stopMonitoringAsync();

        //EVENTS//
        event EventHandler<IRefreshData> refreshed;
    }
}
