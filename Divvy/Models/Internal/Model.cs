﻿using Divvy.Models.Internal.Json;
using Newtonsoft.Json;
using Nito.AsyncEx;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Divvy.Models.Internal
{
    internal class Model : IModel
    {
        //SETTINGS//
        private const string stationInfoURL = "https://feeds.divvybikes.com/stations/stations.json";
        private const int stationID = 98;

        //CONSTRUCTOR//
        internal Model() { }

        //METHODS//
        public async Task startMonitoringAsync()
        {
            using (await monitorLock.LockAsync().ConfigureAwait(false))
            {
                if (monitoring == false)
                {
                    //Start the monitoring task.
                    monitorTokenSource = new CancellationTokenSource();
                    monitorTask = Task.Run(() => monitor(monitorTokenSource.Token), monitorTokenSource.Token);
                    monitoring = true;
                }
            }
        }
        public async Task stopMonitoringAsync()
        {
            using (await monitorLock.LockAsync().ConfigureAwait(false))
            {
                if (monitoring == true)
                {
                    //Stop the monitoring task.
                    monitorTokenSource.Cancel();
                    await monitorTask.ConfigureAwait(false);

                    //Dispose resources.
                    monitorTokenSource.Dispose();
                    monitorTokenSource = null;
                    monitorTask.Dispose();
                    monitorTask = null;
                    monitoring = false;
                }
            }
        }
        private async Task monitor(CancellationToken token)
        {
            WebClient client;
            string stationInfoString;
            StationInfo stationInfo;
            RefreshData refreshData;

            while (true)
            {
                try
                {
                    try
                    {
                        using (client = new WebClient())
                        {
                            stationInfoString = await client.DownloadStringTaskAsync(stationInfoURL).ConfigureAwait(false);
                        }

                        stationInfo = JsonConvert.DeserializeObject<StationInfo>(stationInfoString);

                        if (stationInfo == null || stationInfo.stationBeanList == null)
                        {
                            refreshData = new RefreshData("The station info was not parsed correctly.");
                        }
                        else
                        {
                            refreshData = null;
                            foreach (StationBean stationBean in stationInfo.stationBeanList)
                            {
                                if (stationBean.id == stationID)
                                {
                                    refreshData = new RefreshData(stationInfo.executionTime, stationBean.stationName, stationBean.availableDocks, stationBean.availableBikes, stationBean.lastCommunicationTime);
                                    break;
                                }
                            }

                            if (refreshData == null)
                            {
                                refreshData = new RefreshData("The station ID was not found.");
                            }
                        }
                    }
                    catch(Exception e)
                    {
                        refreshData = new RefreshData(e.Message);
                    }
                    
                    refreshed?.Invoke(this, refreshData);
                    await Task.Delay(15000, token).ConfigureAwait(false);
                }
                catch (OperationCanceledException)
                {
                    return;
                }
                catch { } //This should never be called.
            }
        }

        //FIELDS//
        private AsyncLock monitorLock = new AsyncLock();
        private bool monitoring = false;
        private CancellationTokenSource monitorTokenSource = null;
        private Task monitorTask = null;

        //EVENTS//
        public event EventHandler<IRefreshData> refreshed;
    }
}
