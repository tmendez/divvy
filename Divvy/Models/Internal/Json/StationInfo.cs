﻿using System;
using System.Collections.Generic;

namespace Divvy.Models.Internal.Json
{
    internal class StationInfo
    {
        //PROPERTIES//
        public DateTime? executionTime { get; set; }
        public List<StationBean> stationBeanList { get; set; }
    }
}
