﻿using System;

namespace Divvy.Models.Internal.Json
{
    internal class StationBean
    {
        //PROPERTIES//
        public int? id { get; set; }
        public string stationName { get; set; }
        public int? availableDocks { get; set; }
        public int? availableBikes { get; set; }
        public DateTime? lastCommunicationTime { get; set; }
    }
}
