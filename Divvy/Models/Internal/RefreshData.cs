﻿using System;

namespace Divvy.Models.Internal
{
    internal class RefreshData : IRefreshData
    {
        //CONSTRUCTOR//
        internal RefreshData(string refreshError)
        {
            //Assign properties.
            error = refreshError;
        }
        internal RefreshData(DateTime? refreshDateTime, string refreshStationName, int? refreshAvailableDocks, int? refreshAvailableBikes, DateTime? refreshCommunicationTime)
        {
            //Assign properties.
            error = null;
            refreshTime = refreshDateTime;
            stationName = refreshStationName;
            availableDocks = refreshAvailableDocks;
            availableBikes = refreshAvailableBikes;
            communicationTime = refreshCommunicationTime;
        }

        //PROPERTIES//
        public string error { get; }
        public DateTime? refreshTime { get; }
        public string stationName { get; }
        public int? availableDocks { get; }
        public int? availableBikes { get; }
        public DateTime? communicationTime { get; }
    }
}
