﻿using Divvy.Models.Internal;

namespace Divvy.Models
{
    public static class ModelFactory
    {
        //STATIC METHODS//
        public static IModel create()
        {
            return new Model();
        }
    }
}
