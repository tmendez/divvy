﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using WPF = System.Windows;

namespace Divvy.Views.AttachedProperties
{
    public class Window
    {
        //DEPENDENCY PROPERTIES//
        public static readonly DependencyProperty LoadedCommandProperty = DependencyProperty.RegisterAttached("LoadedCommand", typeof(ICommand), typeof(Window), new PropertyMetadata(null, loadedCommandChanged));
        public static readonly DependencyProperty ClosingCommandProperty = DependencyProperty.RegisterAttached("ClosingCommand", typeof(ICommand), typeof(Window), new PropertyMetadata(null, closingCommandChanged));

        //METHODS//
        private static void loadedCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EventHandler<RoutedEventArgs> handler;

            if (d is WPF.Window)
            {
                //Create the handler.
                handler = null;
                if (e.NewValue is ICommand)
                {
                    handler = (object sender, RoutedEventArgs args) =>
                    {
                        if (((ICommand)e.NewValue).CanExecute(args) == true)
                        {
                            ((ICommand)e.NewValue).Execute(args);
                        }
                    };
                }

                //Set the handler.
                setWindowHandler((WPF.Window)d, handler, loadedHandlerIndex, WindowLoadedEventManager.instance);
            }
        }
        private static void closingCommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            EventHandler<CancelEventArgs> handler;

            if (d is WPF.Window)
            {
                //Create the handler.
                handler = null;
                if (e.NewValue is ICommand)
                {
                    handler = (object sender, CancelEventArgs args) =>
                    {
                        if (((ICommand)e.NewValue).CanExecute(args) == true)
                        {
                            ((ICommand)e.NewValue).Execute(args);
                        }
                    };
                }

                //Set the handler.
                setWindowHandler((WPF.Window)d, handler, closingHandlerIndex, WindowClosingEventManager.instance);
            }
        }
        private static void setWindowHandler(WPF.Window window, Delegate handler, int handlerIndex, IWindowEventManager windowEventManager)
        {
            WeakReference<WPF.Window> windowReference;
            bool hasHandlers;

            //Get the reference.
            windowReference = getWindowReference(window);

            //Remove any old handlers.
            if (windowReference != null)
            {
                if (windowHandlerDictionary[windowReference][handlerIndex] != null)
                {
                    windowEventManager.removeHandler(window, windowHandlerDictionary[windowReference][handlerIndex]);
                    windowHandlerDictionary[windowReference][handlerIndex] = null;
                }
            }

            if(handler != null)
            {
                //Add the new handler.
                if (windowReference == null)
                {
                    windowReference = new WeakReference<WPF.Window>(window);
                    windowHandlerDictionary.Add(windowReference, new Delegate[handlerCount]);
                }
                windowHandlerDictionary[windowReference][handlerIndex] = handler;
                windowEventManager.addHandler(window, handler);
            }
            else
            {
                //Remove the dictionary item if we have no more handlers.
                if (windowReference != null)
                {
                    hasHandlers = false;
                    foreach (Delegate nextHandler in windowHandlerDictionary[windowReference])
                    {
                        if (nextHandler != null)
                        {
                            hasHandlers = true;
                            break;
                        }
                    }
                    if (hasHandlers == false)
                    {
                        windowHandlerDictionary.Remove(windowReference);
                    }
                }
            }
        }
        private static WeakReference<WPF.Window> getWindowReference(WPF.Window source)
        {
            WPF.Window window;
            WeakReference<WPF.Window> windowReference;
            List<WeakReference<WPF.Window>> removedWindowReferences;

            windowReference = null;
            removedWindowReferences = new List<WeakReference<WPF.Window>>();
            foreach (KeyValuePair<WeakReference<WPF.Window>, Delegate[]> entry in windowHandlerDictionary)
            {
                if (entry.Key.TryGetTarget(out window) == true)
                {
                    if(window == source)
                    {
                        windowReference = entry.Key;
                    }
                }
                else
                {
                    removedWindowReferences.Add(entry.Key);
                }
            }
            foreach(WeakReference<WPF.Window> removedWindowReference in removedWindowReferences)
            {
                windowHandlerDictionary.Remove(removedWindowReference);
            }
            return windowReference;
        }

        //FIELDS//
        private const int loadedHandlerIndex = 0;
        private const int closingHandlerIndex = 1;
        private const int handlerCount = 2;
        private static readonly Dictionary<WeakReference<WPF.Window>, Delegate[]> windowHandlerDictionary = new Dictionary<WeakReference<WPF.Window>, Delegate[]>();

        //PROPERTIES//
        public static ICommand GetLoadedCommand(DependencyObject target)
        {
            return (ICommand)target.GetValue(LoadedCommandProperty);
        }
        public static void SetLoadedCommand(DependencyObject target, ICommand value)
        {
            target.SetValue(LoadedCommandProperty, value);
        }
        public static ICommand GetClosingCommand(DependencyObject target)
        {
            return (ICommand)target.GetValue(ClosingCommandProperty);
        }
        public static void SetClosingCommand(DependencyObject target, ICommand value)
        {
            target.SetValue(ClosingCommandProperty, value);
        }

        //INTERFACES//
        private interface IWindowEventManager
        {
            //METHODS//
            void addHandler(WPF.Window source, Delegate handler);
            void removeHandler(WPF.Window source, Delegate handler);
        }

        //CLASSES//
        private class WindowLoadedEventManager : WeakEventManager, IWindowEventManager
        {
            //STATIC PROPERTIES//
            public static WindowLoadedEventManager instance
            {
                get
                {
                    WindowLoadedEventManager manager;

                    //Get the current manager.
                    manager = (WindowLoadedEventManager)WeakEventManager.GetCurrentManager(typeof(WindowLoadedEventManager));

                    //Create a new one if we must.
                    if (manager == null)
                    {
                        manager = new WindowLoadedEventManager();
                        WeakEventManager.SetCurrentManager(typeof(WindowLoadedEventManager), manager);
                    }

                    //Return the manager.
                    return manager;
                }
            }

            //CONSTRUCTOR//
            private WindowLoadedEventManager() { }

            //METHODS//
            public void addHandler(WPF.Window source, Delegate handler)
            {
                if (source == null) { throw new ArgumentNullException(nameof(source)); }
                if (handler == null) { throw new ArgumentNullException(nameof(handler)); }
                if ((handler is EventHandler<RoutedEventArgs>) == false) { throw new ArgumentException("Delegate is of an incorrect type.", nameof(handler)); }

                base.ProtectedAddHandler(source, handler);
            }
            public void removeHandler(WPF.Window source, Delegate handler)
            {
                if (source == null) { throw new ArgumentNullException(nameof(source)); }
                if (handler == null) { throw new ArgumentNullException(nameof(handler)); }
                if ((handler is EventHandler<RoutedEventArgs>) == false) { throw new ArgumentException("Delegate is of an incorrect type.", nameof(handler)); }

                base.ProtectedRemoveHandler(source, handler);
            }
            protected override ListenerList NewListenerList()
            {
                return new ListenerList<RoutedEventArgs>();
            }
            protected override void StartListening(object source)
            {
                ((WPF.Window)source).Loaded += new RoutedEventHandler(windowLoaded);
            }
            protected override void StopListening(object source)
            {
                ((WPF.Window)source).Loaded -= new RoutedEventHandler(windowLoaded);
            }
            private void windowLoaded(object sender, RoutedEventArgs e)
            {
                base.DeliverEvent(sender, e);
            }
        }
        private class WindowClosingEventManager : WeakEventManager, IWindowEventManager
        {
            //STATIC PROPERTIES//
            public static WindowClosingEventManager instance
            {
                get
                {
                    WindowClosingEventManager manager;

                    //Get the current manager.
                    manager = (WindowClosingEventManager)WeakEventManager.GetCurrentManager(typeof(WindowClosingEventManager));

                    //Create a new one if we must.
                    if (manager == null)
                    {
                        manager = new WindowClosingEventManager();
                        WeakEventManager.SetCurrentManager(typeof(WindowClosingEventManager), manager);
                    }

                    //Return the manager.
                    return manager;
                }
            }

            //CONSTRUCTOR//
            private WindowClosingEventManager() { }

            //METHODS//
            public void addHandler(WPF.Window source, Delegate handler)
            {
                if (source == null) { throw new ArgumentNullException(nameof(source)); }
                if (handler == null) { throw new ArgumentNullException(nameof(handler)); }
                if ((handler is EventHandler<CancelEventArgs>) == false) { throw new ArgumentException("Delegate is of an incorrect type.", nameof(handler)); }

                base.ProtectedAddHandler(source, handler);
            }
            public void removeHandler(WPF.Window source, Delegate handler)
            {
                if (source == null) { throw new ArgumentNullException(nameof(source)); }
                if (handler == null) { throw new ArgumentNullException(nameof(handler)); }
                if ((handler is EventHandler<CancelEventArgs>) == false) { throw new ArgumentException("Delegate is of an incorrect type.", nameof(handler)); }

                base.ProtectedRemoveHandler(source, handler);
            }
            protected override ListenerList NewListenerList()
            {
                return new ListenerList<CancelEventArgs>();
            }
            protected override void StartListening(object source)
            {
                ((WPF.Window)source).Closing += new CancelEventHandler(windowClosing);
            }
            protected override void StopListening(object source)
            {
                ((WPF.Window)source).Closing -= new CancelEventHandler(windowClosing);
            }
            private void windowClosing(object sender, CancelEventArgs e)
            {
                base.DeliverEvent(sender, e);
            }
        }
    }
}
