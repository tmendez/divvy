﻿using Divvy.Models;

namespace Divvy.ViewModels
{
    public class ErrorViewModel
    {
        //CONSTRUCTOR//
        internal ErrorViewModel(IRefreshData refreshData)
        {
            //Assign properties.
            data = refreshData;
        }

        //FIELDS//
        private readonly IRefreshData data;

        //PROPERTIES//
        public bool visible
        {
            get
            {
                if(data == null || data.error == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        public string message
        {
            get
            {
                if(data == null || data.error == null)
                {
                    return "";
                }
                else
                {
                    return data.error + " The information above may not be accurate.";
                }
            }
        }
    }
}
