﻿using Divvy.Models;
using Divvy.ViewModels.Commands;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Divvy.ViewModels
{
    public class ViewModel : INotifyPropertyChanged
    {
        //CONSTRUCTOR//
        public ViewModel()
        {
            //Assign propeties.
            loadedCommand = new AsyncCommand(loadedAsync);
            closingCommand = new AsyncCommand(closingAsync);
        }

        //METHODS//
        private void notifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private async Task loadedAsync(object parameter)
        {
            try
            {
                model.refreshed += modelRefreshed;
                await model.startMonitoringAsync();
            }
            catch(Exception e)
            {
                MessageBox.Show("There was an error running the command: " + e.Message);
            }
        }
        private async Task closingAsync(object parameter)
        {
            try
            {
                model.refreshed -= modelRefreshed;
                await model.stopMonitoringAsync();
            }
            catch(Exception e)
            {
                MessageBox.Show("There was an error running the command: " + e.Message);
            }
        }
        private void modelRefreshed(object sender, IRefreshData e)
        {
            try
            {
                context.Post((object state) =>
                {
                    try
                    {
                        if(e.error == null)
                        {
                            station = new StationViewModel(e);
                        }
                        error = new ErrorViewModel(e);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("There was an error running the command: " + ex.Message);
                    }

                }, null);
            }
            catch { } //This should never be called.
        }

        //FIELDS//
        private readonly SynchronizationContext context = SynchronizationContext.Current;
        private readonly IModel model = ModelFactory.create();
        private StationViewModel stationObject = new StationViewModel(null);
        private ErrorViewModel errorObject = new ErrorViewModel(null);

        //PROPERTIES//
        public ICommand loadedCommand { get; }
        public ICommand closingCommand { get; }
        public StationViewModel station
        {
            get
            {
                return stationObject;
            }
            private set
            {
                stationObject = value;
                notifyPropertyChanged();
            }
        }
        public ErrorViewModel error
        {
            get
            {
                return errorObject;
            }
            private set
            {
                errorObject = value;
                notifyPropertyChanged();
            }
        }

        //EVENTS//
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
