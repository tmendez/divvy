﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Divvy.ViewModels.Commands
{
    internal class AsyncCommand : ICommand
    {
        //CONSTRUCTOR//
        public AsyncCommand(Func<object, Task> commandExecuteMethod, Func<object, bool> commandCanExecuteMethod = null)
        {
            executeMethod = commandExecuteMethod ?? throw new ArgumentNullException(nameof(commandExecuteMethod));
            canExecuteMethod = commandCanExecuteMethod;
        }

        //METHODS//
        private void notifyCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }
        public async void Execute(object parameter)
        {
            try
            {
                await executeMethod(parameter);
            }
            catch { }//Exception here will crash our program.
        }
        public bool CanExecute(object parameter)
        {
            if (canExecuteMethod == null)
            {
                return true;
            }
            else
            {
                return canExecuteMethod(parameter);
            }
        }

        //FIELDS//
        private readonly Func<object, Task> executeMethod;
        private readonly Func<object, bool> canExecuteMethod;

        //EVENTS//
        public event EventHandler CanExecuteChanged;
    }
}
