﻿using Divvy.Models;

namespace Divvy.ViewModels
{
    public class StationViewModel
    {
        //CONSTRUCTOR//
        internal StationViewModel(IRefreshData refreshData)
        {
            //Assign properties.
            data = refreshData;
        }

        //FIELDS//
        private readonly IRefreshData data;

        //PROPERTIES//
        public string name
        {
            get
            {
                if (data == null || data.stationName == null)
                {
                    return "-";
                }
                else
                {
                    return data.stationName;
                }
            }
        }
        public string availableDocks
        {
            get
            {
                if (data == null || data.availableDocks == null)
                {
                    return "-";
                }
                else
                {
                    return data.availableDocks.ToString();
                }
            }
        }
        public string availableBikes
        {
            get
            {
                if (data == null || data.availableBikes == null)
                {
                    return "-";
                }
                else
                {
                    return data.availableBikes.ToString();
                }
            }
        }
        public string refreshTime
        {
            get
            {
                if (data == null || data.refreshTime == null)
                {
                    return "-";
                }
                else
                {
                    return data.refreshTime.ToString();
                }
            }
        }
        public string communicationTime
        {
            get
            {
                if (data == null || data.communicationTime == null)
                {
                    return "-";
                }
                else
                {
                    return data.communicationTime.ToString();
                }
            }
        }
    }
}
